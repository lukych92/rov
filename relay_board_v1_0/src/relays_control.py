#!/usr/bin/env python

import rospy
from std_msgs.msg import Bool

import relay



def callback(data, args):
	no_of_relay = args[0]
	if data.data==True:
		rospy.loginfo("Relay "+str(no_of_relay)+" is ON")
		module.ON(no_of_relay)
	else:
		rospy.loginfo("Relay "+str(no_of_relay)+" is OFF")
		module.OFF(no_of_relay)

def listener():
	rospy.init_node('relay_control')
	global module
	module = relay.Relay()

	for i in range(1,5):
		rospy.Subscriber("/relay/control/{}".format(i), Bool, callback, (i,))

	rospy.spin()

if __name__ == '__main__':
	listener()
