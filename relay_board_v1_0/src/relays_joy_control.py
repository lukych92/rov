#!/usr/bin/env python

import rospy
from std_msgs.msg import Bool
from sensor_msgs.msg import Joy




def callback(data):
	global pubs
	# print("{} {} {} {} {} {}".format(data.buttons[8],
	# 								 data.buttons[9],
	# 								 data.buttons[10],
	# 								 data.buttons[11],
	# 								 data.buttons[12],
	# 								 data.buttons[13]))
	if data.buttons[9] == 1:
		msg = Bool()
		msg.data = True
		pubs[1].publish(msg)
	if data.buttons[8] == 1:
		msg = Bool()
		msg.data = False
		pubs[1].publish(msg)

	if data.buttons[11] == 1:
		msg = Bool()
		msg.data = True
		pubs[2].publish(msg)
	if data.buttons[10] == 1:
		msg = Bool()
		msg.data = False
		pubs[2].publish(msg)

	if data.buttons[13] == 1:
		msg = Bool()
		msg.data = True
		pubs[3].publish(msg)
	if data.buttons[12] == 1:
		msg = Bool()
		msg.data = False
		pubs[3].publish(msg)


def listener():
	global pubs
	rospy.init_node('relay_joy_control')

	pubs = []
	for i in range(1,5):
		pubs.append(rospy.Publisher("/relay/control/{}".format(i), Bool, queue_size=10))

	rospy.Subscriber("/joy", Joy, callback)

	rospy.spin()

if __name__ == '__main__':
	listener()
