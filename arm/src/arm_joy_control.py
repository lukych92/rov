#!/usr/bin/env python

import rospy
from arm.msg import SetMotorStates, MotorState
from sensor_msgs.msg import Joy


axes_remmaper = { 0 : [3, 'no-reversed'],
				  1 : [5, 'no-reversed'],
				  2 : [6, 'reversed'],
				  3 : [1, 'reversed'],
				  4 : [4, 'reversed']
				  }

def callback(data):
	global pub
	for joint in axes_remmaper:
		key = axes_remmaper[joint][0]
		mode = axes_remmaper[joint][1]
		# rospy.logwarn(value)
		msg = SetMotorStates()
		state = MotorState()
		state.motorIdx = joint
		if data.axes[key] > 0 :
			if mode == 'reversed':
				state.motorState = 2
			else:
				state.motorState = 1
		if data.axes[key] < 0:
			if mode == 'reversed':
				state.motorState = 1
			else:
				state.motorState = 2
		if data.axes[key] == 0:
			state.motorState = 0
		msg.motorStates.append(state)
		pub.publish(msg)


def listener():
	global pub
	rospy.init_node('relay_joy_control')

	rospy.Subscriber("/joy_arm", Joy, callback)
	pub = rospy.Publisher("setArmMotorStates", SetMotorStates, queue_size=10)

	rospy.spin()

if __name__ == '__main__':
	listener()
