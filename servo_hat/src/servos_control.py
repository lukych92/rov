#!/usr/bin/python

import rospy

from std_msgs.msg import Int32

from Adafruit_PWM_Servo_Driver import PWM

def callback(data, args):
	pwm.setPWM(args[0],0,data.data/5)

def listener():
	global pwm
	pwm = PWM(0x40)
	pwm.setPWMFreq(45)
	
	rospy.init_node('servos_control')
	for i in range(16):
		rospy.Subscriber("/servos/control/{}".format(i), Int32, callback, (i,))
		pwm.setPWM(i, 0, 1500/ 5)

	rospy.spin()

if __name__ == '__main__':
	listener()
