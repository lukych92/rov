#!/usr/bin/env python

import os
import rospy
import rospkg

from std_msgs.msg import Int32
from std_msgs.msg import Bool

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtWidgets import QWidget




class ServoHatGUI(Plugin):

    def __init__(self, context):
        super(ServoHatGUI, self).__init__(context)

        # Give QObjects reasonable names
        self.setObjectName('ServoHatGUI')

        # Process standalone plugin command-line arguments
        # from argparse import ArgumentParser
        # parser = ArgumentParser()
        # # Add argument(s) to the parser.
        # parser.add_argument("-q", "--quiet", action="store_true",
        #                     dest="quiet",
        #                     help="Put plugin in silent mode")
        # args, unknowns = parser.parse_known_args(context.argv())
        # if not args.quiet:
        #     print 'arguments: ', args
        #     print 'unknowns: ', unknowns

        # Create QWidget
        self._widget = QWidget()
        # Get path to UI file which should be in the "resource" folder of this package
        ui_file = os.path.join(rospkg.RosPack().get_path('servo_hat'), 'resource', 'gui.ui')
        # Extend the widget with all attributes and children from UI file
        loadUi(ui_file, self._widget)
        # Give QObjects reasonable names
        self._widget.setObjectName('ServoHatGUI')
        # Show _widget.windowTitle on left-top of each plugin (when
        # it's set in _widget). This is useful when you open multiple
        # plugins at once. Also if you open multiple instances of your
        # plugin at once, these lines add number to make it easy to
        # tell from pane to pane.
        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() + (' (%d)' % context.serial_number()))
        # Add widget to the user interface
        context.add_widget(self._widget)


        self.CHANEL = [self._widget.CHANEL_0,
                       self._widget.CHANEL_1,
                       self._widget.CHANEL_2,
                       self._widget.CHANEL_3,
                       self._widget.CHANEL_4,
                       self._widget.CHANEL_5,
                       self._widget.CHANEL_6,
                       self._widget.CHANEL_7,
                       self._widget.CHANEL_8,
                       self._widget.CHANEL_9,
                       self._widget.CHANEL_10,
                       self._widget.CHANEL_11,
                       self._widget.CHANEL_12,
                       self._widget.CHANEL_13,
                       self._widget.CHANEL_14,
                       self._widget.CHANEL_15
                       ]

        self.NAME = [self._widget.NAME_0,
                     self._widget.NAME_1,
                     self._widget.NAME_2,
                     self._widget.NAME_3,
                     self._widget.NAME_4,
                     self._widget.NAME_5,
                     self._widget.NAME_6,
                     self._widget.NAME_7,
                     self._widget.NAME_8,
                     self._widget.NAME_9,
                     self._widget.NAME_10,
                     self._widget.NAME_11,
                     self._widget.NAME_12,
                     self._widget.NAME_13,
                     self._widget.NAME_14,
                     self._widget.NAME_15
                     ]


        self.RELAY = [self._widget.relay1Box,
                      self._widget.relay2Box,
                      self._widget.relay3Box,
                      self._widget.relay4Box
                      ]


        for i in range(len(self.CHANEL)):
            self.CHANEL[i].valueChanged.connect(self.publish_servos_msg)
            self.CHANEL[i].valueChanged.connect(self.name_set_num)

        self.RELAY[0].clicked.connect(lambda: self.publish_relay_msg(0))
        self.RELAY[1].clicked.connect(lambda: self.publish_relay_msg(1))
        self.RELAY[2].clicked.connect(lambda: self.publish_relay_msg(2))
        self.RELAY[3].clicked.connect(lambda: self.publish_relay_msg(3))

        self.name_set_num()
        self.publish_servos_msg()


        for i in range(len(self.RELAY)):
            self.publish_relay_msg(i)

    def name_set_num(self):
        for i in range(len(self.NAME)):
            self.NAME[i].setNum(self.CHANEL[i].value())


    def publish_servos_msg(self):
        for i in range(len(self.CHANEL)):
            pub = rospy.Publisher('/servos/control/{}'.format(i), Int32 ,queue_size=10)
            msg = Int32()
            msg.data = int(self.CHANEL[i].value())
            pub.publish(msg)


    def publish_relay_msg(self, i):
        pub = rospy.Publisher('/relay/control/{}'.format(i+1), Bool, queue_size=1)
        msg = Bool()
        msg.data = bool(self.RELAY[i].isChecked())
        pub.publish(msg)