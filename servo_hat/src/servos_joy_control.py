#!/usr/bin/env python

import rospy
from std_msgs.msg import Int32
from sensor_msgs.msg import Joy




def callback(data):
	global pubs
	msg = Int32()

	msg.data = 1500+(data.axes[3]*500)
	pubs[14].publish(msg)

	msg.data = 1500+(data.axes[4]*500)
	pubs[15].publish(msg)

def listener():
	global pubs
	rospy.init_node('servos_joy_control')

	pubs = []
	for i in range(16):
		pubs.append(rospy.Publisher("/servos/control/{}".format(i), Int32, queue_size=10))

	rospy.Subscriber("/joy", Joy, callback)

	rospy.spin()

if __name__ == '__main__':
	listener()
